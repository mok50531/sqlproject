import org.testng.Assert;
import utils.ConfigReader;

import java.sql.*;

public class DataBaseConnection {

    public static void main(String[] args) throws SQLException {

        String hostname = ConfigReader.getProperty("bdHostName");
        String username = ConfigReader.getProperty("bdUsername");
        String password = ConfigReader.getProperty("bdPassword");
        String PayamQuery = "select first_name, last_name, employee_id, salary from employees where manager_id = (select employee_id from employees where first_name = 'Payam')";

        //creating connection to the database with the parameters
        Connection connection = DriverManager.getConnection(hostname, username, password);
        System.out.println("System should be able to connect to the database");
        //statement keeps the connection between my machine and database
        Statement statement = connection.createStatement();
        //  Sending the PayamQuery to database
        ResultSet resultSet = statement.executeQuery(PayamQuery);

        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

        System.out.println("Number of column: " + resultSetMetaData.getColumnCount());
        System.out.println("Name of first column: " + resultSetMetaData.getColumnName(1));
        System.out.println(" " + resultSetMetaData.getColumnDisplaySize(1));

        while (resultSet.next()) {
            String firstName = resultSet.getString("FIRST_NAME");
            String lastName = resultSet.getString("LAST_NAME");
            System.out.println(firstName + " " + lastName);

            if (firstName.equals("Steven")) {
                String actualName = firstName;
                Assert.assertEquals(actualName, "Steven");
                System.out.println("Actual name: " + actualName);
                break;
            }

        }
    }
}
