package stepDef;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import utils.DatabaseUtils;

import java.util.List;

public class DatabaseStepDef {

    private final String query = "SELECT first_name, last_name from employees where manager_id = (select employee_id from employees where first_name = 'Payam')";

    @Given("User is able to connect to database")
    public void user_is_able_to_connect_to_database() {
        DatabaseUtils.createDBConnection();
    }

    @When("User sends the {string} to database")
    public void user_sends_the_to_database(String query) {
        DatabaseUtils.executeQuery(query);
    }

    @Then("validate information taken from the database displayed correct with the order")
    public void validate_information_taken_from_the_database_displayed_correct_with_the_order(DataTable dataTable) {
        List<List<String>> list = dataTable.asLists();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 2; j++) {
                Assert.assertEquals(list.get(i).get(j), DatabaseUtils.getQueryResultList(query).get(i).get(j));
            }
        }
    }
}
